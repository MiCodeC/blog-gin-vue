package utils

import (
	"fmt"
	"os"
	"serve/config"
	"strings"

	"github.com/google/uuid"
)

// 生成随机文章id
func GenFileId() string {
	u, _ := uuid.NewRandom()
	return fmt.Sprintf("%d", u.ID())
}

// 获取md文件路径
func getContentFilePath(fileName string) string {
	return config.Article_Md_SavePath + "/" + fileName + "." + config.Article_Save_ExName
}

// 获取文章html文件路径
func getHtmlFilePath(fileName string) string {
	return config.Article_Html_SavePath + "/" + fileName + "." + config.Article_Save_ExName
}

// 写入文件
func writeToFile(str string, filePath string) error {
	f, err := os.OpenFile(filePath, os.O_RDWR|os.O_TRUNC|os.O_CREATE, 0644)
	if err != nil {
		return err
	}
	defer f.Close()
	sr := strings.NewReader(str)
	sr.WriteTo(f)
	return nil
}

// 保存文章相关文件
func SaveArticleFiles(content string, html string, fileid string) error {
	// 保存markdown内容
	filepath := getContentFilePath(fileid)
	err := writeToFile(content, filepath)
	if err != nil {
		return err
	}
	// 保存转译后的html内容
	filepath = getHtmlFilePath(fileid)
	err = writeToFile(html, filepath)
	if err != nil {
		return err
	}
	return nil
}

func GetContentFile(fileid string) (string, error) {
	filepath := getContentFilePath(fileid)
	content, err := os.ReadFile(filepath)
	if err != nil {
		return "", err
	}
	return string(content), nil
}

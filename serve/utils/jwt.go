package utils

import (
	"serve/config"
	"time"

	"github.com/dgrijalva/jwt-go"
)

type Claims struct {
	Uuid string `json:"uuid"`
	jwt.StandardClaims
}

// 获取MD5加密后的token密钥
func getMD5ScretKey() []byte {
	return []byte(GetMd5String(config.JWT_SecretKey))
}

// 获取Token
func GenJWTString(param ...string) string {
	jwtCliams := Claims{
		Uuid: param[0],
		StandardClaims: jwt.StandardClaims{
			NotBefore: time.Now().Unix(),                           // 生效实践
			ExpiresAt: time.Now().Add(config.JWT_ExpiresAt).Unix(), // 有效期
			Issuer:    config.JWT_Issuer,                           // 签发者
		},
	}
	tokenObj := jwt.NewWithClaims(jwt.SigningMethodHS256, jwtCliams)
	skey := getMD5ScretKey()
	token, err := tokenObj.SignedString(skey)
	if err != nil {
		return ""
	}
	return token
}

// 解析Token
func ParseToken(token string) (*Claims, error) {
	skey := getMD5ScretKey()
	tokenObj, err := jwt.ParseWithClaims(token, &Claims{}, func(t *jwt.Token) (interface{}, error) {
		return skey, nil
	})
	if err != nil {
		return nil, err
	}
	claims := tokenObj.Claims.(*Claims)
	return claims, nil
}

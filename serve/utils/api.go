package utils

import (
	"serve/config"

	"github.com/gin-gonic/gin"
)

// 响应数据结构类型
type ApiSendData struct {
	Msg  config.ApiResponse
	Data interface{}
}

// 下发json格式影响数据
func ApiSendJson(ctx *gin.Context, res ApiSendData) {
	ctx.JSON(200, gin.H{
		"code":    res.Msg.Code,
		"message": res.Msg.Message,
		"data":    res.Data,
	})
}

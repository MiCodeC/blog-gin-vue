package utils

import (
	"errors"
	"fmt"
	"reflect"
	"serve/config"
	"strconv"
)

// 参考例子，目前仅支持配置 int ~ int64、uint ~ uint64、byte=uint8、float32、float64、string、bool
// type MyType struct {
// 	Index  int     `init:"100"`
// 	Name   string  `init:"micodec"`
// 	Number int64   `init:"1000"`
// 	Fper   float32 `init:"3.7"`
// }

// 使用泛型：如果有传入的实参，那么就使用实参初始化字段，没有就使用tag标签属性配置
func NewStruct[T any](params ...interface{}) (T, error) {
	var instance T // 初始化结构体实例

	// 获取实例的类型
	typeOfT := reflect.TypeOf(instance)
	// 判断是否结构体，如果不是那么直接返回一个默认实例对象
	if typeOfT.Kind().String() != "struct" {
		err := errors.New(config.NStructErr_NotStruct)
		return instance, err
	}

	// 获取结构体反射值对象
	values := reflect.ValueOf(&instance).Elem()

	// 遍历结构体成员
	for i := 0; i < typeOfT.NumField(); i++ {
		// 获取结构体成员字段反射对象
		field := typeOfT.Field(i)
		// 获取成员字段的反射值对象
		fieldValue := values.FieldByName(field.Name)

		// 创建用于存储初始化值的对象
		var initValue interface{}

		// 判断是否有通过形参传入初始化值，没有就使用标签属性的配置值
		if len(params) >= i+1 {
			initValue = params[i]
			// 检查构造参数与字段的类型是否相符
			if field.Type.Name() != reflect.TypeOf(initValue).Name() {
				errMsg := fmt.Sprintf(config.NStructErr_TypeErr, field.Name)
				return instance, errors.New(errMsg)
			}
		} else {
			initValue = field.Tag.Get("init") // 这里得到的标签属性值都是string类型
		}

		if reflect.TypeOf(initValue).Name() == "string" {
			errMsg := fmt.Sprintf(config.NStructErr_TagErr, field.Name)
			// 字符串类型，表示函数参数类型是字符串或者使用标签配置作为初始值
			switch field.Type.Name() {
			case "int", "int8", "int16", "int32", "int64":
				tmp, err := strconv.ParseInt(initValue.(string), 10, 64)
				if err != nil {
					return instance, errors.New(errMsg)
				}
				fieldValue.SetInt(tmp)
			case "uint", "uint8", "uint16", "uint32", "uint64":
				tmp, err := strconv.ParseUint(initValue.(string), 10, 64)
				if err != nil {
					return instance, errors.New(errMsg)
				}
				fieldValue.SetUint(tmp)
			case "float32", "float64":
				tmp, err := strconv.ParseFloat(initValue.(string), 64)
				if err != nil {
					return instance, errors.New(errMsg)
				}
				fieldValue.SetFloat(tmp)
			case "bool":
				tmp, err := strconv.ParseBool(initValue.(string))
				if err != nil {
					return instance, errors.New(errMsg)
				}
				fieldValue.SetBool(tmp)
			case "string":
				// 如果是字符串类型，直接将其断言为string，直接使用
				fieldValue.SetString(initValue.(string))
			}
		} else {
			// 将构造参数值赋给字段
			fieldValue.Set(reflect.ValueOf(initValue))
		}
	}

	// 返回初始化后的实例
	return instance, nil
}

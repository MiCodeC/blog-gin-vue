package utils

import (
	"crypto/md5"
	"fmt"
)

// MD5加密并返回字符串
func GetMd5String(b []byte) string {
	return fmt.Sprintf("%x", md5.Sum(b))
}

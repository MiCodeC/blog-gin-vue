package routers

import (
	"serve/api"
	"serve/middleware"

	"github.com/gin-gonic/gin"
)

// 路由规则配置
func InitApiRouer(router *gin.Engine) {
	adminRouter := router.Group("/adminapi")
	adminRouter.Use(middleware.JwtAuth())
	{
		adminRouter.POST("/user/login", api.UserLogin)
		adminRouter.POST("/user/logout", api.UserLogOut)
		adminRouter.GET("/user/userinfo", api.UserInfo)
		adminRouter.POST("/channel/add", api.ChannelAdd)
		adminRouter.GET("/channel/all", api.ChannelAll)
		adminRouter.GET("/channel/del/:id", api.ChannelDel)
		adminRouter.POST("/channel/update", api.ChannelUpdate)
		adminRouter.POST("/article/upload", api.ArticelUpload)
		adminRouter.GET("/article/get/:fileid", api.ArticleGet)
	}
}

package main

import (
	"net/http"
	"serve/global"
	"serve/initialize"
	"time"
)

func main() {
	// 初始化 gorm
	global.GOLBAL_DB = initialize.Gorm()

	if global.GOLBAL_DB != nil {
		initialize.MigrateTables(global.GOLBAL_DB)

		// 关闭数据库
		db, _ := global.GOLBAL_DB.DB()
		defer db.Close()
	}

	// 创建默认管理员
	initialize.CreateAdmin()

	// 初始化 router
	router := initialize.Router()

	adminServe := &http.Server{
		Addr:         ":8081",
		Handler:      router,
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
	}

	adminServe.ListenAndServe()
}

package initialize

import (
	"serve/config"
	"serve/model"
	"serve/utils"
	"time"

	"github.com/google/uuid"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

// 初始化gorm数据库对象
func Gorm() *gorm.DB {
	db, err := gorm.Open(mysql.New(mysql.Config{
		DSN:               config.SQL_DSN,
		DefaultStringSize: 256, // string类型字段的默认长度
	}), &gorm.Config{
		DisableForeignKeyConstraintWhenMigrating: true,
	})

	if err != nil {
		return nil
	}

	sqlDB, _ := db.DB()

	// 设置数据库连接池
	sqlDB.SetMaxIdleConns(10)           // 空闲连接数量
	sqlDB.SetMaxOpenConns(10)           // 设置打开数据库连接的最大数量
	sqlDB.SetConnMaxLifetime(time.Hour) // 设置连接生命周期

	return db
}

// 自动迁移
func MigrateTables(db *gorm.DB) {
	db.AutoMigrate(
		&model.User{},
		&model.Article{},
		&model.Channel{},
	)
}

// 创建初始管理员
func CreateAdmin() {
	password := utils.GetMd5String([]byte(config.Password))
	uuid, _ := uuid.NewRandom()
	admin := model.User{
		Uuid:     uuid.String(),
		Username: config.Username,
		Password: password,
	}

	// 检查是否存在默认账户
	rowsAffected := admin.NameUnique()

	// 如果不存在默认账户则创建
	if rowsAffected == 0 {
		admin.CreateUser()
	}
}

package initialize

import (
	"serve/routers"

	"github.com/gin-gonic/gin"
)

// 路由初始化
func Router() *gin.Engine {
	router := gin.New()
	router.Use(gin.Recovery())
	router.Use(gin.Logger())
	{
		routers.InitApiRouer(router)
	}
	return router
}

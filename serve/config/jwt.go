package config

import "time"

var (
	JWT_ExpiresAt = time.Hour * 24                    // 有效期
	JWT_Issuer    = "admin"                           // token签发者
	JWT_SecretKey = []byte("woshiscretkeyjiamimiyao") // token签名密钥配置
)

// 不需要token的请求
var JWT_Filters = []string{
	"/adminapi/user/login",
	"/adminapi/user/logout",
}

package config

var (
	Article_Md_SavePath   = "./files/markdowns" // markdown源存储目录
	Article_Html_SavePath = "./files/htmls"     // markdown转html存储目录
	Article_Save_ExName   = "tmpl"              // 保存文件类型
)

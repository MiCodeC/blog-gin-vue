package config

type ApiResponse struct {
	Code    int
	Message string
}

var (
	API_Login_Fail             = ApiResponse{Code: 400, Message: "登录失败"}
	API_Login_Success          = ApiResponse{Code: 200, Message: "登录成功"}
	API_Logout_Success         = ApiResponse{Code: 200, Message: "登出成功"}
	API_Userinfo_Fail          = ApiResponse{Code: 400, Message: "获取用户信息失败"}
	API_Userinfo_Success       = ApiResponse{Code: 200, Message: "获取用户信息成功"}
	API_ChannelAll_Fail        = ApiResponse{Code: 400, Message: "获取频道列表失败"}
	API_ChannelAll_Success     = ApiResponse{Code: 200, Message: "获取频道列表成功"}
	API_ChannelAdd_ParamErr    = ApiResponse{Code: 400, Message: "频道名非法，检查是否为空或者重复"}
	API_ChannelAdd_Fail        = ApiResponse{Code: 400, Message: "添加频道操作失败"}
	API_ChannelAdd_Success     = ApiResponse{Code: 200, Message: "添加频道操作成功"}
	API_ChannelDel_ParamErr    = ApiResponse{Code: 400, Message: "删除频道请求参数错误"}
	API_ChannelDel_Fail        = ApiResponse{Code: 400, Message: "删除频道操作失败"}
	API_ChannelDel_Success     = ApiResponse{Code: 200, Message: "删除频道操作成功"}
	API_ChannelUpdate_ParamErr = ApiResponse{Code: 400, Message: "更新频道请求参数错误"}
	API_ChannelUpdate_Fail     = ApiResponse{Code: 400, Message: "更新频道操作失败"}
	API_ChannelUpdate_Success  = ApiResponse{Code: 200, Message: "更新频道操作成功"}
	API_ArticleUpload_ParamErr = ApiResponse{Code: 400, Message: "上传文章请求参数错误"}
	API_ArticleUpload_FileErr  = ApiResponse{Code: 400, Message: "上传文章文件操作错误"}
	API_ArticleUpload_Fail     = ApiResponse{Code: 400, Message: "上传文章保存失败"}
	API_ArticleUpload_Success  = ApiResponse{Code: 200, Message: "上传文章保存成功"}
	API_ArticleGet_ParamErr    = ApiResponse{Code: 400, Message: "获取文章请求参数错误"}
	API_ArticleGet_Fail        = ApiResponse{Code: 400, Message: "获取文章失败"}
	API_ArticleGet_Success     = ApiResponse{Code: 200, Message: "获取文章成功"}
	API_ArticleGet_FileErr     = ApiResponse{Code: 200, Message: "无法获取文章的Markdown文档"}
	JWT_GetTokenErr            = ApiResponse{Code: 400, Message: "token无法解析或者失效，请重新登录"}
)

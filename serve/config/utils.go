package config

var (
	NStructErr_NotStruct = "泛型T不是结构体，无法构造"
	NStructErr_TagErr    = "字段 %s 标签配置值无法转换成字段类型"
	NStructErr_TypeErr   = "传入的初始化值与字段 %s 类型不匹配"
)

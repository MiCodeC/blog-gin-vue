package api

import (
	"fmt"
	"serve/api/response"
	"serve/config"
	"serve/model"
	"serve/utils"
	"strings"

	"github.com/gin-gonic/gin"
)

type reqArticleGet struct {
	Fileid string `uri:"fileid"`
}

func ArticleGet(ctx *gin.Context) {
	var req reqArticleGet
	err := ctx.ShouldBindUri(&req)
	if err != nil {
		utils.ApiSendJson(ctx, utils.ApiSendData{
			Msg: config.API_ArticleGet_ParamErr,
		})
		return
	}
	article := model.Article{
		Fileid: strings.Trim(req.Fileid, " "),
	}
	fmt.Printf("article: %v\n", article)
	isExist := article.GetArticleByFileid()
	if !isExist {
		utils.ApiSendJson(ctx, utils.ApiSendData{
			Msg: config.API_ArticleGet_Fail,
		})
		return
	}
	content, err := utils.GetContentFile(article.Fileid)
	if err != nil {
		utils.ApiSendJson(ctx, utils.ApiSendData{
			Msg: config.API_ArticleGet_FileErr,
		})
		return
	}
	res := response.Article{
		Title:     article.Title,
		Fileid:    article.Fileid,
		Tags:      article.Tags,
		ChannelID: article.ChannelID,
		Content:   content,
	}
	utils.ApiSendJson(ctx, utils.ApiSendData{
		Msg:  config.API_ArticleGet_Success,
		Data: res,
	})
}

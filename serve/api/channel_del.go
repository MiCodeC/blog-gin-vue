package api

import (
	"serve/api/response"
	"serve/config"
	"serve/model"
	"serve/utils"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

// 请求参数结构体
type reqChannelDel struct {
	ID uint `uri:"id"`
}

func ChannelDel(ctx *gin.Context) {
	var req reqChannelDel
	err := ctx.ShouldBindUri(&req)
	if err != nil {
		utils.ApiSendJson(ctx, utils.ApiSendData{
			Msg: config.API_ChannelDel_ParamErr,
		})
		return
	}
	channel := model.Channel{
		Model: gorm.Model{
			ID: req.ID,
		},
	}
	// 执行删除操作
	err = channel.DelChannelById()
	if err != nil {
		utils.ApiSendJson(ctx, utils.ApiSendData{
			Msg: config.API_ChannelDel_Fail,
		})
		return
	}
	// 返回新的频道数据列表
	var res []response.Channel
	err = channel.GetChannelList(&res)
	if err != nil {
		utils.ApiSendJson(ctx, utils.ApiSendData{
			Msg: config.API_ChannelAll_Fail,
		})
		return
	}
	utils.ApiSendJson(ctx, utils.ApiSendData{
		Msg:  config.API_ChannelAdd_Success,
		Data: res,
	})

}

package api

import (
	"serve/config"
	"serve/utils"

	"github.com/gin-gonic/gin"
)

// 登出回调函数
func UserLogOut(ctx *gin.Context) {
	utils.ApiSendJson(ctx, utils.ApiSendData{
		Msg: config.API_Logout_Success,
	})
}

package api

import (
	"serve/api/response"
	"serve/config"
	"serve/model"
	"serve/utils"
	"strings"

	"github.com/gin-gonic/gin"
)

// 请求参数结构体
type reqChannelAdd struct {
	Name string `json:"name"`
}

// 添加新频道回调
func ChannelAdd(ctx *gin.Context) {
	var req reqChannelAdd
	// 检查参数是否有值
	err := ctx.ShouldBindJSON(&req)
	if err != nil || req.Name == "" {
		utils.ApiSendJson(ctx, utils.ApiSendData{
			Msg: config.API_ChannelAdd_ParamErr,
		})
		return
	}
	// 待插入频道对象
	channel := model.Channel{
		Name: strings.Trim(req.Name, " "),
	}
	// 检测是否有同名频道
	rowsAffected := channel.NameUnique()
	if rowsAffected != 0 {
		utils.ApiSendJson(ctx, utils.ApiSendData{
			Msg: config.API_ChannelAdd_ParamErr,
		})
		return
	}
	// 插入数据库
	err = channel.CreateChannel()
	if err != nil {
		utils.ApiSendJson(ctx, utils.ApiSendData{
			Msg: config.API_ChannelAdd_Fail,
		})
		return
	}
	// 返回新的频道数据列表
	var res []response.Channel
	err = channel.GetChannelList(&res)
	if err != nil {
		utils.ApiSendJson(ctx, utils.ApiSendData{
			Msg: config.API_ChannelAll_Fail,
		})
		return
	}
	utils.ApiSendJson(ctx, utils.ApiSendData{
		Msg:  config.API_ChannelAdd_Success,
		Data: res,
	})

}

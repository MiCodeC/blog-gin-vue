package api

import (
	"serve/api/response"
	"serve/config"
	"serve/model"
	"serve/utils"

	"github.com/gin-gonic/gin"
)

// 获取用户信息回调
func UserInfo(ctx *gin.Context) {
	uuid, _ := ctx.Get("uuid")
	use := model.User{
		Uuid: uuid.(string),
	}
	var res response.User
	// 通过uuid查找用户信息
	result := use.GetUserByUuid(&res)
	if !result {
		utils.ApiSendJson(ctx, utils.ApiSendData{
			Msg: config.API_Userinfo_Fail,
		})
		return
	}
	utils.ApiSendJson(ctx, utils.ApiSendData{
		Msg:  config.API_Userinfo_Success,
		Data: res,
	})

}

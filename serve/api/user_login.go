package api

import (
	"serve/config"
	"serve/model"
	"serve/utils"

	"github.com/gin-gonic/gin"
)

// 登录请求参数
type reqLogin struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

// 登录回调
func UserLogin(ctx *gin.Context) {
	var req reqLogin
	err := ctx.ShouldBindJSON(&req)
	if err != nil {
		utils.ApiSendJson(ctx, utils.ApiSendData{
			Msg: config.API_Login_Fail,
		})
		return
	}
	pwd := utils.GetMd5String([]byte(req.Password))
	// 待验证用户对象
	var use = model.User{
		Username: req.Username,
		Password: pwd,
	}
	// 验证登录账户
	result := use.CheckLogin()
	if !result {
		utils.ApiSendJson(ctx, utils.ApiSendData{
			Msg: config.API_Login_Fail,
		})
		return
	}
	data := map[string]string{
		"x-token": utils.GenJWTString(use.Uuid),
	}
	utils.ApiSendJson(ctx, utils.ApiSendData{
		Msg:  config.API_Login_Success,
		Data: data,
	})

}

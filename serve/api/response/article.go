package response

type Article struct {
	Title     string
	Fileid    string
	Tags      string
	ChannelID uint
	Content   string
}

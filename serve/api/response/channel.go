package response

// 响应频道数据结构类型
type Channel struct {
	ID   uint
	Name string
}

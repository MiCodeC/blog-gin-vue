package api

// 请求结构体
type reqArticleList struct {
	PageSize    uint
	CurrentPage uint
}

// 获取文章列表
func NewReqArticleList(params ...interface{}) reqArticleList {
	if params == nil {
		return reqArticleList{
			PageSize:    10,
			CurrentPage: 1,
		}
	} else if len(params) == 1 {
		return reqArticleList{
			PageSize:    params[0].(uint),
			CurrentPage: 1,
		}
	} else {
		return reqArticleList{}
	}
}

package api

import (
	"serve/config"
	"serve/model"
	"serve/utils"
	"strings"

	"github.com/gin-gonic/gin"
)

// 上传文章请求参数
type reqArticleUpload struct {
	Fileid  string `json:"fileid"`
	Channel int    `json:"channel"`
	Title   string `json:"title"`
	Content string `json:"content"`
	Html    string `json:"html"`
}

// 上传文章
func ArticelUpload(ctx *gin.Context) {
	var req reqArticleUpload
	err := ctx.ShouldBindJSON(&req)
	// 检查请求参数是否符合最低要求
	if err != nil || req.Channel <= 0 || req.Title == "" || req.Content == "" || req.Html == "" {
		utils.ApiSendJson(ctx, utils.ApiSendData{
			Msg: config.API_ArticleUpload_ParamErr,
		})
		return
	}
	article := model.Article{
		Title:     strings.Trim(req.Title, " "),
		ChannelID: uint(req.Channel),
	}
	// 判断请求是否存在fileid，存在则更新，否则新增
	if req.Fileid == "" {
		// 新增
		article.Fileid = utils.GenFileId()
		err = article.CreateArticle()
	} else {
		// 更新
		article.Fileid = req.Fileid
		err = article.UpdateByFileid()
	}
	if err != nil {
		utils.ApiSendJson(ctx, utils.ApiSendData{
			Msg: config.API_ArticleUpload_Fail,
		})
		return
	}
	// 保存文章的内容
	err = utils.SaveArticleFiles(req.Content, req.Html, article.Fileid)
	if err != nil {
		utils.ApiSendJson(ctx, utils.ApiSendData{
			Msg: config.API_ArticleUpload_FileErr,
		})
		return
	}
	utils.ApiSendJson(ctx, utils.ApiSendData{
		Msg:  config.API_ArticleUpload_Success,
		Data: article,
	})

}

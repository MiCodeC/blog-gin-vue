package api

import (
	"serve/api/response"
	"serve/config"
	"serve/model"
	"serve/utils"

	"github.com/gin-gonic/gin"
)

// 获取频道列表回调
func ChannelAll(ctx *gin.Context) {
	var channel = model.Channel{}
	var res []response.Channel
	// 查找所有频道信息
	err := channel.GetChannelList(&res)
	if err != nil {
		utils.ApiSendJson(ctx, utils.ApiSendData{
			Msg: config.API_ChannelAll_Fail,
		})
		return
	}
	utils.ApiSendJson(ctx, utils.ApiSendData{
		Msg:  config.API_ChannelAll_Success,
		Data: res,
	})

}

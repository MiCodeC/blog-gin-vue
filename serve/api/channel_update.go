package api

import (
	"serve/api/response"
	"serve/config"
	"serve/model"
	"serve/utils"

	"github.com/gin-gonic/gin"
)

// 请求参数结构体
type reqChannelUpdate struct {
	ID   uint   `json:"id"`
	Name string `json:"name"`
}

func ChannelUpdate(ctx *gin.Context) {
	var req reqChannelUpdate
	err := ctx.ShouldBindJSON(&req)
	if err != nil {
		utils.ApiSendJson(ctx, utils.ApiSendData{
			Msg: config.API_ChannelUpdate_ParamErr,
		})
		return
	}
	var channel model.Channel
	channel.ID = req.ID
	channel.Name = req.Name
	// 执行更新操作
	err = channel.UpdateById()
	if err != nil {
		utils.ApiSendJson(ctx, utils.ApiSendData{
			Msg: config.API_ChannelUpdate_Fail,
		})
		return
	}
	// 返回新的频道数据列表
	var res []response.Channel
	err = channel.GetChannelList(&res)
	if err != nil {
		utils.ApiSendJson(ctx, utils.ApiSendData{
			Msg: config.API_ChannelAll_Fail,
		})
		return
	}
	utils.ApiSendJson(ctx, utils.ApiSendData{
		Msg:  config.API_ChannelUpdate_Success,
		Data: res,
	})
}

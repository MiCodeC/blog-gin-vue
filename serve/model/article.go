package model

import (
	"serve/global"

	"gorm.io/gorm"
)

// 文章表模型
type Article struct {
	gorm.Model
	Title     string `gorm:"not null"`
	Fileid    string `gorm:"not null;unique"`
	Tags      string
	ChannelID uint `gorm:"not null"`
}

// 创建频道
func (a *Article) CreateArticle() error {
	return global.GOLBAL_DB.Create(a).Error
}

// 创建频道
func (a *Article) UpdateByFileid() error {
	return global.GOLBAL_DB.Model(a).Where("fileid = ?", a.Fileid).Updates(a).Error
}

// 根据fileid获取文章记录
func (a *Article) GetArticleByFileid() bool {
	result := global.GOLBAL_DB.Model(a).Where("fileid = ?", a.Fileid).Find(a)
	return result.RowsAffected != 0
}

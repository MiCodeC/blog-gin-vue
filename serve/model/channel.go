package model

import (
	"serve/global"

	"gorm.io/gorm"
)

// 频道表模型
type Channel struct {
	gorm.Model
	Name     string    `gorm:"not null;unique"`
	Articles []Article // 一对多关联关系
}

// 检查模型对象的用户名是否唯一，返回受影响行数
func (c *Channel) NameUnique() int64 {
	result := global.GOLBAL_DB.Where("name = ?", c.Name).Find(&Channel{})
	return result.RowsAffected
}

// 创建频道
func (c *Channel) CreateChannel() error {
	return global.GOLBAL_DB.Create(c).Error
}

// 获取频道列表
func (c *Channel) GetChannelList(saveObj interface{}) error {
	result := global.GOLBAL_DB.Model(c).Find(saveObj)
	return result.Error
}

// 根据id删除频道
func (c *Channel) DelChannelById() error {
	return global.GOLBAL_DB.Unscoped().Delete(c).Error
}

// 根据id更新指定列表
func (c *Channel) UpdateById() error {
	return global.GOLBAL_DB.Model(c).Updates(c).Error
}

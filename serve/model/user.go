package model

import (
	"serve/global"
	"time"

	"gorm.io/gorm"
)

// 基于 gorm.model 修改的用户表模式
type User struct {
	Uuid      string `gorm:"primarykey;not null;unique"`
	Username  string `gorm:"not null;unique"`
	Password  string `gorm:"not null"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt gorm.DeletedAt `gorm:"index"`
}

// 检查模型对象的用户名是否唯一，返回受影响行数
func (u *User) NameUnique() int64 {
	result := global.GOLBAL_DB.Where("username = ?", u.Username).Find(&User{})
	return result.RowsAffected
}

// 创建用户
func (u *User) CreateUser() {
	global.GOLBAL_DB.Create(u)
}

// 验证用户信息，返回 true 表示获取到用户记录
func (u *User) CheckLogin() bool {
	result := global.GOLBAL_DB.Where("username = ? and password = ?", u.Username, u.Password).First(u)
	return result.RowsAffected != 0
}

// 通过uuid获取用户信息，返回 true 表示获取到用户记录
func (u *User) GetUserByUuid(saveObj interface{}) bool {
	result := global.GOLBAL_DB.Where("uuid = ?", u.Uuid).First(saveObj)
	return result.RowsAffected != 0
}

package middleware

import (
	"serve/config"
	"serve/utils"

	"github.com/gin-gonic/gin"
)

type Token struct {
	Token string `header:"X-Token"`
}

// 路径过滤：是否要经过JWT验证
func jwtCheckReqPath(path *string) bool {
	for _, f := range config.JWT_Filters {
		if f == *path {
			return true
		}
	}
	return false
}

// JWT 验证
func JwtAuth() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		path := ctx.Request.URL.Path
		var token Token
		ctx.ShouldBindHeader(&token)
		// 路径过滤
		if jwtCheckReqPath(&path) {
			ctx.Next()
		} else {
			// 解析token
			claims, err := utils.ParseToken(token.Token)
			if err != nil {
				utils.ApiSendJson(ctx, utils.ApiSendData{
					Msg: config.JWT_GetTokenErr,
				})
				ctx.Abort()
			} else {
				ctx.Set("uuid", claims.Uuid) // 通过token存储用户的uuid
				ctx.Next()
			}
		}
	}
}

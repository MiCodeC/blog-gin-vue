import { mapActions } from "vuex"
export const mixin_GetChannels = {
  mounted() {
    this.getChannels();
  },
  methods: {
    ...mapActions("channel", [
      "getChannelsAction",
    ]),
    async getChannels() {
      try {
        await this.getChannelsAction();
      } catch (err) {
        this.$message.error(err.message);
      }
    },
  }
}
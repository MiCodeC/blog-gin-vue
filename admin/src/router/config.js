/* 路由配置 */

export default [
  {
    path: "/articles",
    name: "文章管理",
    component: () => import("@/views/articles"),
    meta: {
      icon: "el-icon-document"
    }
  },{
    path: "/channels",
    name: "频道管理",
    component: () => import("@/views/channels"),
    meta: {
      icon: "el-icon-folder"
    }
  },{
    path: "/mdeditor",
    name: "编辑文章",
    component: () => import("@/views/mdedit"),
    meta: {
      icon: "el-icon-edit-outline"
    }
  }
]
import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

import layout from "@/layout"
import subRouters from "./config"

const routes = [
  {
    path: "/login",
    name: "login",
    component: () => import('@/views/login')
  },
  {
    path: "/",
    name: "layout",
    component: layout,
    redirect: "articles",
    children: subRouters
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router

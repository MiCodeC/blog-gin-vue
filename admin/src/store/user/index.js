import { reqUserLogin, reqGetUserInfo, reqUserLogout } from "@/api/user";
import { setToken, getToken, removeToken } from "@/utils/token";

const state = {
  token: getToken(),
  userinfo: {}
}
const mutations = {
  // 存储token
  USER_LOGIN(state, responseData){
    state.token = responseData
  },

  // 存储用户信息
  USER_INFO(state, responseData) {
    state.userinfo = responseData
  },

  // 清除登录数据
  USER_LOGOUT(state) {
    state.token = ""
    state.userinfo = {}
  }
}
const actions = {
  // 登录
  async loginAction({commit}, loginInfo) {
    let result = await reqUserLogin(loginInfo)
    if (result.code === 200) {
      setToken(result.data["x-token"])
      commit("USER_LOGIN", result.data["x-token"])
      return Promise.resolve(result.code)
    } else {
      return Promise.reject(result)
    }
  },

  // 请求用户信息
  async getUserInfoAction({commit}) {
    let result = await reqGetUserInfo()
    if (result.code === 200) {
      commit("USER_INFO", result.data)
      return Promise.resolve(result.code)
    } else {
      return Promise.reject(result) 
    }
  },

  // 退出登录
  async logoutAction({commit}) {
    let result = await reqUserLogout()
    if (result.code === 200) {
      commit("USER_LOGOUT")
      removeToken()
      return Promise.resolve(result.code)
    } else {
      return Promise.reject(result)
    }
  }
}

const getters = {}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
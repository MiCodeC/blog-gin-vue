import { reqChannelAdd, reqDelChannel, reqGetChannels, reqUpdateChannel } from "@/api/channel"

const state = {
  channels: []
}
const mutations = {
  SAVE_CHANNELS(state, data){
    state.channels = data
  }
}
const actions = {
  // 添加频道
  async addChannelAction({commit} ,data) {
    let result =  await reqChannelAdd(data)
    if (result.code === 200) {
      commit("SAVE_CHANNELS", result.data)
      return Promise.resolve(result.code)
    } else {
      return Promise.reject(result)
    }
  },

  // 请求所有频道信息
  async getChannelsAction({commit}){
    let result = await reqGetChannels()
    if (result.code === 200) {
      commit("SAVE_CHANNELS", result.data)
      return Promise.resolve(result.code)
    } else {
      return Promise.reject(result)
    }
  },

  // 删除指定频道
  async delChannelAction({commit}, id) {
    let result = await reqDelChannel(id)
    if (result.code === 200) {
      commit("SAVE_CHANNELS", result.data)
      return Promise.resolve(result.code)
    } else {
      return Promise.reject(result)
    }
  },

  // 更新指定频道
  async updateChannelAction({commit}, data) {
    let result = await reqUpdateChannel(data)
    if (result.code === 200) {
      commit("SAVE_CHANNELS", result.data)
      return Promise.resolve(result.code)
    } else {
      return Promise.reject(result)
    }
  }
}
const getters = {}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
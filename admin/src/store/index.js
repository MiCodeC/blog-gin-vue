import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import user from "./user"
import channel from "./channel"
import article from "./articles"
import getters from "./getters"

export default new Vuex.Store({
  modules: {
    user,
    channel,
    article
  },
  getters
})

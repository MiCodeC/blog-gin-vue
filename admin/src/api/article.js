import requests from "@/utils/requests";

// 上传文章
export const reqArticleUpload = (data) => {
  return requests({
    url: `/article/upload`,
    method: "POST",
    data
  })
}

// 获取文章
export const reqGetArticle = (fileid) => {
  return requests({
    url: `/article/get/${fileid}`,
    method: "GET"
  })
}
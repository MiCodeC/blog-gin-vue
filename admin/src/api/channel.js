import requests from "@/utils/requests";

/* 新增频道 */
export const reqChannelAdd = (data) => {
  return requests({
    url: `/channel/add`,
    method: "POST",
    data
  })
}

/* 获取所有频道 */
export const reqGetChannels = () => {
  return requests({
    url: `/channel/all`,
    method: "GET"
  })
}

/* 删除指定频道 */
export const reqDelChannel = (id) => {
  return requests({
    url: `/channel/del/${id}`,
    method: "GET"
  })
}

/* 更新指定频道 */
export const reqUpdateChannel = (data) => {
  return requests({
    url: `/channel/update`,
    method: "POST",
    data: data
  })
}
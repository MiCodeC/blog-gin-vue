import requests from "@/utils/requests";

/* 用户登录请求 */
export const reqUserLogin = (data) => {
  return requests({
    url: `/user/login`,
    data,
    method: "POST"
  })
}

/* 退出登录 */
export const reqUserLogout = () => {
  return requests({
    url: `/user/logout`,
    method: "POST"
  })
}

/* 请求用户信息 */
export const reqGetUserInfo = () => {
  return requests({
    url: `/user/userinfo`,
    method: "GET"
  })
}
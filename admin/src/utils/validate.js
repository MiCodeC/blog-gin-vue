export const validateUsername = (rule, value, callback) => {
  const pattern = /^[A-Za-z][_A-Za-z\d]{3,}$/
  if (value == "") {
    callback(new Error("用户名不能为空"))
  } else if (String(value).search(pattern) == -1) {
    callback(new Error("用户名非法"))
  } else {
    callback()
  }
}

export const validatePassword = (rule, value, callback) => {
  const pattern = /^[A-Za-z\d_@]{8,}$/
  if (value == "") {
    callback(new Error("用户名不能为空"))
  } else if (String(value).search(pattern) == -1) {
    callback(new Error("密码字符非法"))
  } else {
    callback()
  }
}
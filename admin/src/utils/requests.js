import axios from 'axios'
import store from "@/store"

const requests = axios.create({
  baseURL: '/adminapi',
  timeout: 5000
})

// 请求拦截器
requests.interceptors.request.use((config) => {
  if (store.getters.token) {
    config.headers['X-Token'] = store.state.user.token
  }
  return config
}, error => {
  console.log(error)
  return Promise.reject(error)
})

// 响应拦截器
requests.interceptors.response.use((res) => {
  return res.data
}, (err) => {
  return Promise.reject(err)
})

export default requests
export const setToken = (token) => {
  localStorage.setItem("X-TOKEN", token)
}

export const getToken = () => localStorage.getItem("X-TOKEN")

export const removeToken = () => localStorage.removeItem("X-TOKEN")